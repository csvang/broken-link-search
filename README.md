# Broken Link Search
  
## Description  
Extending Adam Saladino's project, Broken Link Search.  
Original:  https://github.com/asaladino/broken-link-search
  
## Install npm Packages
npm install  
  
## Start Electron App
npm start

## To Do  
(many improvements to come...)  
* Code
    * Move function calls into classes
    * Need to further review broken-link-checker and integrate API's as needed.
* Bug - Fix recursion issue
* UI
    * Start scan button toggle to Start/Stop
    * Status of scan