const blc = require("broken-link-checker");

// let linkCheckRepository = new LinkCheckRepository('list.csv');

// let linkChecks = [];
// linkCheckRepository.findAll().then(links => {
//     linkChecks = links;
//     links.forEach(linkCheck => {
//         htmlUrlChecker.enqueue(linkCheck.url, linkCheck);
//     });
// });

var objURLs = (sessionStorage.getItem("blsURLs")) ? JSON.parse(sessionStorage.getItem("blsURLs")) : [];

var btnAddURL = document.getElementById('btnAddURL');
var btnScan = document.getElementById('btnScan');

var txtURL = document.getElementById('txtURL');
var delURL = document.getElementsByClassName('delURL');

var URLItems = document.getElementById('URLItems');
var lstURLItem = URLItems.getElementsByTagName("li");

var lstBrokenURLs = document.getElementById('lstBrokenURLs');

txtURL.addEventListener('keyup', function(event){ 
    if (event.keyCode == 13) {
        addURLtoList(); 
    }
})

this.getURLList(objURLs);

btnAddURL.addEventListener('click',function(){ addURLtoList(); })

btnScan.addEventListener('click', function(){ startScan(objURLs); })

URLItems.addEventListener('click', function(e){ // Use event delegation
    if(e.target && e.target.matches(".delURL")){
        //this.removeChild(e.srcElement.parentNode);

        removeURL(objURLs, e.srcElement.parentElement.getAttribute('data-url'));

        if (lstURLItem.length <= 0){
            btnScan.style.display = "none";
        }
    }
    //alert(e.target);
})

// var brokenURLs = [];
// var checkerURL = "";
// let htmlUrlChecker = new blc.HtmlUrlChecker({}, {
//     'link': function (result, customData) {
//         if (result.broken) {
//             //console.log(result.base.original + " => " + result.url.original);
//             // customData.addBroken(result.url.original);
           
        
//             brokenURLs.push(result.url.original);
//             //alert(brokenURLs);
//             checkerURL = result.base.original;
//         }
//     },
//     'end': function () {
//         console.log("Saving");
//         btnScan.disabled = false;
        
//         addURLtoBrokenList(objURLs, checkerURL, brokenURLs);
//         brokenURLs = [];
//         //linkCheckRepository.write(linkChecks);
//     }
// });

let htmlUrlChecker = new blc.HtmlUrlChecker({}, {
    'link': function (result, customData) {
        if (result.broken) {
            //console.log(result.base.original + " => " + result.url.original);
            // customData.addBroken(result.url.original);
            addURLtoBrokenList(objURLs, result.base.original, result.url.original);

        }
    },
    'end': function () {
        console.log("Saving");
        btnScan.disabled = false;
        //linkCheckRepository.write(linkChecks);
    }
});

//var txtTest = document.getElementById('txtTest');
function addURLtoBrokenList(lstURL, origURL, brokenURL){
    if (lstURL.length > 0){
        for ( var x = 0 ; x <= lstURL.length - 1; x++){
            //txtTest.value = '"' + lstURL[x].URL + '" + "' + origURL + '"';
            
            if (lstURL[x].URL.trim() === origURL.trim()) { 
                var brokenURLs = (lstURL[x].brokenurls) ? lstURL[x].brokenurls : [];

                brokenURLs.push(brokenURL);
                lstURL[x].brokenurls = brokenURLs; //.push(url)
                
            }
        }
    }

    sessionStorage.setItem("blsURLs", JSON.stringify(lstURL));
    getURLList(objURLs);
}

function addURLtoList(){
    if (txtURL.value.trim() && !URLExist(objURLs, txtURL.value.trim())){
        
        objURLs.push({
            'URL': txtURL.value.trim()
        })

        sessionStorage.setItem("blsURLs", JSON.stringify(objURLs));
        this.getURLList(objURLs);

    } else {

        if (URLExist(objURLs, txtURL.value)){
            alert("URL Exists: \r\n" + txtURL.value);
        }

    }

    txtURL.focus();
}

function getURLList(lstURL){
    if (lstURL){
        URLItems.innerHTML="";
        lstURL.forEach(function(url){
            var domURLItem = document.createElement('li');
            domURLItem.setAttribute('data-url', url.URL);

            var delURL = document.createElement('span');
            delURL.className = 'delURL fa fa-times';
            
            domURLItem.appendChild(delURL);
            domURLItem.appendChild(document.createTextNode(url.URL));

            // UL - Broken UL List
            var domULBrokenLinks = document.createElement('ul');
            domULBrokenLinks.className = 'ulBrokenLinks';
            
            if (url.brokenurls && url.brokenurls.length > 0){
                url.brokenurls.forEach(function(brokenURL){
                    var domLIBrokenLinks = document.createElement('li');
    
                    domLIBrokenLinks.appendChild(document.createTextNode(brokenURL));
                    domULBrokenLinks.appendChild(domLIBrokenLinks);

                })
            }

            domURLItem.appendChild(domULBrokenLinks);
            
            URLItems.appendChild(domURLItem);
            txtURL.value = '';
            
            if (lstURLItem.length > 0){
                btnScan.style.display = "block";
            }
        });
    }
}

function URLExist(lstURL, urlCompare){
    if (lstURL.length > 0){
        for ( var x = 0 ; x <= lstURL.length - 1; x++){
            if (lstURL[x].URL === urlCompare) { return true; }
        }
    }

    return false;
}


function removeURL(lstURL, urlRemove){
    if (lstURL.length > 0){
        for ( var x = 0 ; x <= lstURL.length - 1; x++){
            if (lstURL[x].URL === urlRemove) { 
                objURLs.splice(x, 1);
                sessionStorage.setItem("blsURLs", JSON.stringify(objURLs));
                getURLList(objURLs);
            }
        }
    }
}

function startScan(lstURL){
    if (lstURL.length > 0){
        resetScan(lstURL);
        btnScan.disabled = true;

        lstURL.forEach(function(url){
            htmlUrlChecker.enqueue(url.URL);
        });

        // for(var x = 0; x <= lstURLItem.length -1; x++){
        //     htmlUrlChecker.enqueue(lstURLItem[x].innerText);
        // }
    }
}

function resetScan(lstURL){
    lstURL.forEach(function(url){
        if (url.brokenurls) {
            url.brokenurls = [];
        }
    });
}